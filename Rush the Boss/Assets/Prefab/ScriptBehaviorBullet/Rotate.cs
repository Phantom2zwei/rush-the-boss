﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
    public float SpeedRotate = 1f;

    void Update()
    {
        transform.Rotate(0, 0, SpeedRotate);
    }
}
