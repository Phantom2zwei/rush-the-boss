﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour
{
	public Image Fade;
	private Rigidbody2D rgb = null;
	private Animator animator = null;
	private List<GameObject> currentCollidedList = new List<GameObject>();
	private bool loading = false;

	public bool hasUpgraded = false;
	private Upgrade currentCollidedUpgrade = null;
	public GameObject upgradeText = null;

	private bool isDead = false;
	public float maxLife = 100.0f;
	[HideInInspector]
	public float currentLife = 1.0f;
	public float Speed = 1f;

	public float JumpForce = 1f;
	private bool jumpStarted = false; // Prevents double jump with collisions

	private bool canMove = false;

	[SerializeField]
	private float dashMaxDuration = 1.0f;
	private float dashDuration = 0.0f;
	[SerializeField]
	private float dashCooldownDuration = 4.0f;
	private float dashCooldown = 0.0f;
	[SerializeField]
	private float dashForce = 3.0f;
	private bool isDashing = false;

	private float invulnerableTime = 0.0f;
	[SerializeField]
	private float onHitInvulnerabilityDuration = 1.0f;

	[SerializeField]
	private Transform shootOrigin = null;
	[SerializeField]
	private float damage = 10.0f;
	[SerializeField]
	private GameObject bulletPrefab = null;
	public List<Transform> Limit = new List<Transform>();
	public Transform replacePlayer;

	public float attackSpeedMultiplier = 1.0f;

	public float scoreMultiplier = 1.0f;
	public int score = 0;

	// Events
	public delegate void DamageTaken(float value);
	public event DamageTaken onDamageTaken;

	public delegate void LifeChanged(float currentLife, float maxLife);
	public event LifeChanged onLifeChanged;

	public delegate void Died();
	public event Died onDeath;

    float DirX;
    float DirY;

    private void Awake()
	{
		currentLife = maxLife;
	}

	void Start()
	{
		rgb = gameObject.GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		UIManager.instance.SetPlayer(this);
	}

	private void Update()
	{
		if (isDead == true && loading == false && (Input.GetAxisRaw("Submit") != 0.0f || Input.GetAxisRaw("Cancel") != 0.0f))
		{
			StartCoroutine(ReturnToMenu());
			SoundManager.instance.PlayValidationSound();
		}
	}

	void FixedUpdate()
	{
		if (canMove == true)
			InputPlayer();
		CheckDashState();

		if (IsOnPlatform() == false)
			animator.SetBool("IsJumping", true);
		else
			animator.SetBool("IsJumping", false);

		invulnerableTime = Mathf.Max(invulnerableTime - Time.fixedDeltaTime, 0.0f);
	}

	public bool CheckLimit(Transform obj)
	{
		if (obj.position.x < Limit[0].position.x || obj.position.x > Limit[1].position.x ||
						obj.position.y > Limit[2].position.y || obj.position.y < Limit[3].position.y)
		{
			return true;
		}
		return false;
	}
	public void ReplacePlayer()
	{
		transform.position = replacePlayer.position;
	}

	public void StartMove()
	{
		canMove = true;
	}

	private void SetInvulnerableTime(float time)
	{
		invulnerableTime = time;
	}

	public bool IsInvulnerable()
	{
		return invulnerableTime > 0.0f;
	}

	private void ShootBullet()
	{
		if (bulletPrefab != null && shootOrigin != null)
		{
			//Vector2 shootDir = new Vector2((Input.GetAxis("AimHorizontal") > 0) ? 1 : -1, (-Input.GetAxis("AimVertical") > 0) ? 1 : -1);
			Vector2 shootDir = new Vector2(transform.position.x + DirX - transform.position.x,
					transform.position.y + (-DirY) - transform.position.y);

			//Vector2 shootDir = new Vector3(Input.GetAxis("AimHorizontal"), -Input.GetAxis("AimVertical"));
			GameObject playerBullet = Instantiate(bulletPrefab, shootOrigin.position, Quaternion.identity, transform.parent);
			if (playerBullet)
			{
				BulletPlayer bulletComp = playerBullet.GetComponent<BulletPlayer>();
				if (bulletComp != null)
				{
					bulletComp.Direction = shootDir;
					bulletComp.damage = damage;
				}
			}
		}
	}

	public void Upgrade(Upgrade.Type type, float value)
	{
		SoundManager.instance.PlayValidationSound();
		if (upgradeText != null)
			upgradeText.gameObject.SetActive(false);

		if (type == global::Upgrade.Type.Attack)
			damage += value;
		else if (type == global::Upgrade.Type.AttackSpeed)
		{
			attackSpeedMultiplier += value;
			if (animator != null)
				animator.SetFloat("AttackSpeedMultiplier", attackSpeedMultiplier);
		}
		else if (type == global::Upgrade.Type.HP)
		{
			maxLife += value;
			currentLife += value;
			onLifeChanged(currentLife, maxLife);
		}
		else if (type == global::Upgrade.Type.Score)
			scoreMultiplier += value;
	}

	void InputPlayer()
	{
		//remove
		if (Input.GetKeyDown(KeyCode.I))
			TakeDamage(15.0f);

		// Upgrade
		if (currentCollidedUpgrade != null && hasUpgraded == false && Input.GetAxis("Fire3") > 0.0f)
		{
			Upgrade(currentCollidedUpgrade.type, currentCollidedUpgrade.valueToAdd);
			hasUpgraded = true;
		}

		// Set shoot state
		if (Input.GetAxis("AimHorizontal") != 0.0f || Input.GetAxis("AimVertical") != 0.0f)
		{
			// Shoot
			animator.SetBool("IsShooting", true);
            DirX = Input.GetAxis("AimHorizontal");
            DirY = Input.GetAxis("AimVertical");
        }
		else
		{
			animator.SetBool("IsShooting", false);
		}

		// Set dash state
		if (Input.GetAxis("Fire1") != 0 && dashCooldown == 0.0f)
		{
			isDashing = true;
			animator.SetBool("IsDashing", true);
			SetInvulnerableTime(dashMaxDuration);
			dashCooldown = dashCooldownDuration;
			dashDuration = 0.0f;
			Physics2D.IgnoreLayerCollision(gameObject.layer, 0, true);
			rgb.AddForce(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * dashForce, ForceMode2D.Impulse);
		}

		// When falling or jump button isnt pressed, activate collisions with platforms
		if (rgb.velocity.y <= 0.0f)
		{
			if (isDashing == false)
				Physics2D.IgnoreLayerCollision(gameObject.layer, 0, false);
			jumpStarted = false;
		}
		// Go through floors
		if (Input.GetAxis("Vertical") < 0.0f && Input.GetAxis("Jump") > 0.0f)
		{
			if (isDashing == false)
				Physics2D.IgnoreLayerCollision(gameObject.layer, 0, true);
		}
		// Jump
		else if (jumpStarted == false && IsOnPlatform() == true && Input.GetAxis("Jump") > 0.0f)
		{
			rgb.AddForce(Vector2.up * JumpForce);
			if (isDashing == false)
				Physics2D.IgnoreLayerCollision(gameObject.layer, 0, true);
			jumpStarted = true;
		}

		// Compute walk and dash translation
		float translationX = Input.GetAxis("Horizontal") * (isDashing ? Speed * dashForce : Speed) * Time.fixedDeltaTime;
		float translationY = isDashing ? Input.GetAxis("Vertical") * Speed * 2.0f * Time.fixedDeltaTime : 0.0f;
		transform.Translate(translationX, translationY, 0);

		// Set anim variable
		animator.SetBool("IsWalking", (translationX != 0.0f) ? true : false);

		// Set sprite direction
		Vector3 newScale = transform.localScale;
		if (translationX > 0.0f)
			newScale.x = Mathf.Abs(newScale.x);
		else if (translationX < 0.0f)
			newScale.x = -Mathf.Abs(newScale.x);
		transform.localScale = newScale;
		if (upgradeText != null)
		{
			Vector3 upTextScale = upgradeText.transform.localScale; // Upgrade text direction
			if (translationX > 0.0f)
				upTextScale.x = Mathf.Abs(upTextScale.x);
			else if (translationX < 0.0f)
				upTextScale.x = -Mathf.Abs(upTextScale.x);
			upgradeText.transform.localScale = upTextScale;
		}
	}

	void CheckDashState()
	{
		if (isDashing)
		{
			dashDuration += Time.fixedDeltaTime;
			if (dashDuration >= dashMaxDuration)
			{
				isDashing = false;
				animator.SetBool("IsDashing", false);
				dashCooldown = dashCooldownDuration;
				Physics2D.IgnoreLayerCollision(gameObject.layer, 0, false);
			}
		}
		else
		{
			dashCooldown = Mathf.Max(dashCooldown - Time.fixedDeltaTime, 0.0f);
			UIManager.instance.SetDashCooldownPercentage((dashCooldownDuration - dashCooldown) / dashCooldownDuration);
		}
	}

	public void TakeDamage(float value)
	{
		if (IsInvulnerable())
			return;

		currentLife = Mathf.Max(currentLife - value, 0.0f);
		SetInvulnerableTime(onHitInvulnerabilityDuration);

		SoundManager.instance.PlayHurtSound();
		if (onDamageTaken != null)
			onDamageTaken(value);
		if (onLifeChanged != null)
			onLifeChanged(currentLife, maxLife);
		if (currentLife <= 0.0f && animator != null)
			animator.SetTrigger("Death");
		else if (animator != null)
			animator.SetTrigger("Hit");
	}

	private void Die()
	{
		//Time.timeScale = 0.0f;
		isDead = true;
		if (onDeath != null)
			onDeath();
	}

	public void AddScore(float value)
	{
		score += (int)(value * scoreMultiplier);
		UIManager.instance.SetPoints(score);
	}

	public bool IsOnPlatform()
	{
		foreach (GameObject collided in currentCollidedList)
		{
			if (collided.tag == "CanStep")
				return true;
		}
		return false;
	}

	public bool IsGrounded()
	{
		foreach (GameObject collided in currentCollidedList)
		{
			if (collided.layer == LayerMask.NameToLayer("GroundLimit") && collided.name == "GroundLimit")
				return true;
		}
		return false;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Upgrade"))
		{
			Upgrade up = collision.gameObject.GetComponent<Upgrade>();
			currentCollidedUpgrade = up;
			if (upgradeText != null && hasUpgraded == false)
				upgradeText.gameObject.SetActive(true);
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Upgrade"))
		{
			currentCollidedUpgrade = null;
			if (upgradeText != null)
				upgradeText.gameObject.SetActive(false);
		}
	}

		void OnCollisionEnter2D(Collision2D other)
	{
		currentCollidedList.Add(other.gameObject);
	}

	private void OnCollisionExit2D(Collision2D collision)
	{
		currentCollidedList.Remove(collision.gameObject);
	}

	IEnumerator ReturnToMenu()
	{
		loading = true;
		yield return new WaitForSeconds(1f);
		//if (Fade != null)
		//	Fade.CrossFadeAlpha(1f, 2f, true);
		//yield return new WaitForSeconds(2f);
		SoundManager.PlayMusic(0);
		SceneManager.LoadScene(1);
	}
}
