﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public static UIManager instance = null;

	private Player player = null;
	private Boss boss = null;
	[SerializeField]
	private Slider playerHpSlider = null;
	[SerializeField]
	private Slider bossHpSlider = null;
	[SerializeField]
	private Text pointsText = null;
	[SerializeField]
	private Text stageText = null;

	[SerializeField]
	private Image dashActiveImage = null;

	// Chakras
	[SerializeField]
	private GameObject chakra1Active = null;
	[SerializeField]
	private GameObject chakra2Active = null;
	[SerializeField]
	private GameObject chakra3Active = null;
	[SerializeField]
	private GameObject chakra4Active = null;
	[SerializeField]
	private GameObject chakra5Active = null;

	[SerializeField]
	private GameObject winScreen = null;
	[SerializeField]
	private GameObject deathScreen = null;
	[SerializeField]
	private Text deathScoreText = null;
	[SerializeField]
	private GameObject gameCanvas = null;
	[SerializeField]
	private Canvas cameraCanvas = null;

	private int lastPoints = 0;


	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
			Destroy(this);
	}

	public void DisplayDeathScreen()
	{
		if (deathScreen != null)
		{
			deathScreen.SetActive(true);
			gameCanvas.SetActive(false);
		}
		if (cameraCanvas != null)
		{
			cameraCanvas.planeDistance = 0.31f;
		}
		if (deathScoreText != null)
			deathScoreText.text = lastPoints.ToString("N0");

        SaveScore score = SaveLoad.Load<SaveScore>("Score");
				if (score == null)
					return;
        for (int i = 0; i < score.Score.Count; i++)
        {
            if (lastPoints > score.Score[i])
            {
                score.Score.Insert(i, lastPoints);
                score.Score.RemoveAt(score.Score.Count - 1);
                SaveLoad.AddOrModifyElementInSaveFile<SaveScore>("Score", score);
                break;
            }
        }
    }

	public void DisplayWinScreen()
	{
		if (winScreen != null)
			winScreen.SetActive(true);
		if (gameCanvas != null)
			gameCanvas.SetActive(false);
		if (cameraCanvas != null)
		{
			cameraCanvas.planeDistance = 0.31f;
		}

        SaveScore score = SaveLoad.Load<SaveScore>("Score");
        for (int i = 0; i < score.Score.Count; i++)
        {
            if (lastPoints > score.Score[i])
            {
                score.Score.Insert(i, lastPoints);
                score.Score.RemoveAt(score.Score.Count - 1);
                SaveLoad.AddOrModifyElementInSaveFile<SaveScore>("Score", score);
                break;
            }
        }
    }

	public void SetPlayer(Player player)
	{
		this.player = player;
		if (player != null)
		{
			SetPlayerHp(player.currentLife, player.maxLife);
			player.onLifeChanged += SetPlayerHp;
			player.onDeath += DisplayDeathScreen;
		}
	}

	public void SetBoss(Boss newBoss)
	{
		boss = newBoss;
		if (boss != null)
		{
			boss.onLifeChanged += SetBossHp;
			SetBossHp(boss.currentLife, boss.maxLife);
		}
	}

	public void SetPlayerHp(float currentLife, float maxLife)
	{
		if (playerHpSlider)
			playerHpSlider.value = currentLife / maxLife;
	}

	public void SetBossHp(float currentLife, float maxLife)
	{
		if (bossHpSlider)
			bossHpSlider.value = currentLife / maxLife;
	}

	public void SetPoints(int points)
	{
		lastPoints = points;
		if (pointsText != null)
			pointsText.text = points.ToString("N0");
	}

	public void SetDashCooldownPercentage(float percentage)
	{
		if (dashActiveImage != null)
		{
			dashActiveImage.fillAmount = percentage;
		}
	}

	public void SetStage(int stage)
	{
		if (stageText != null)
			stageText.text = "Stage " + stage.ToString();

		if (chakra1Active != null && stage == 1)
			chakra1Active.SetActive(true);
		else if (chakra2Active != null && stage == 2)
			chakra2Active.SetActive(true);
		else if (chakra3Active != null && stage == 3)
			chakra3Active.SetActive(true);
		else if (chakra4Active != null && stage == 4)
			chakra4Active.SetActive(true);
		else if (chakra5Active != null && stage == 5)
			chakra5Active.SetActive(true);
	}
}
