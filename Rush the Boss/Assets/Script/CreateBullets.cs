﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class CreateBullets : MonoBehaviour
{
	public Transform LimitLeft;
	public Transform LimitUp;
	public Transform LimitRight;
	public Transform LimitDown;
	public Transform MoveTo;
	public bool Teleport = false;
	public float TimeToGo = 10;
    public List<InstantiateBullet> ListOfBullet = new List<InstantiateBullet>();

	private List<Transform> LBullet = new List<Transform>();

	private Transform Boss;
	private Vector2 vel = Vector2.zero;
	[HideInInspector]
	public bool MoveBoss = false;
    [HideInInspector]
    public float Repeat = 0f;
    public float RepeatEverySec = 0f;

    void Start()
	{
		if (GameObject.FindObjectOfType<Boss> ()) 
		{
			Boss = GameObject.FindObjectOfType<Boss> ().transform;
			if (Teleport)
				Boss.position = MoveTo.position;
		}
	}

    public void InstBull()
    {
        for (int i = 0; i < ListOfBullet.Count; i++)
        {
            var temp = Instantiate<GameObject>(ListOfBullet[i].PrefabBullet);
            if (ListOfBullet[i].Spawn != null)
                temp.transform.position = ListOfBullet[i].Spawn.position;
            else
                temp.transform.position = Boss.position;

            temp.GetComponent<Bullet>().Direction = ListOfBullet[i].GetDir();
            LBullet.Add(temp.transform);
        }
    }

	void Update()
	{
        if (Repeat > RepeatEverySec)
        {
            InstBull();
            Repeat = 0;
        }
        if (Boss && MoveBoss)
        {
			if (Teleport)
				Boss.position = MoveTo.position;
			else 
			{
				var XY = Vector2.SmoothDamp (new Vector2 (Boss.position.x, Boss.position.y),
					new Vector2 (MoveTo.position.x, MoveTo.position.y), ref vel, TimeToGo, 1, 1);
				Boss.position = new Vector3 (XY.x,XY.y, Boss.position.z);
			}
		}
		for (int i = 0; i < LBullet.Count; i++) 
		{
			if (LBullet[i].transform.position.x < LimitLeft.position.x ||
				LBullet[i].transform.position.x > LimitRight.position.x ||
				LBullet[i].transform.position.y > LimitUp.position.y ||
				LBullet[i].transform.position.y < LimitDown.position.y)
			{
				Destroy (LBullet[i].gameObject);
				LBullet.RemoveAt (i);
				i = 0;
			}
		}
	}
}

[Serializable]
public class InstantiateBullet
{
    public Transform Spawn;
    public GameObject PrefabBullet;
    public Direction dir;
    private Vector2 Direction = Vector2.zero;

    public Vector2 GetDir()
    {
        if (dir.ToString() == "UpLeft")
            return Direction = Vector2.up + Vector2.left;
        else if (dir.ToString() == "Up")
            return Direction = Vector2.up;
        else if (dir.ToString() == "UpRight")
            return Direction = Vector2.up + Vector2.right;
        else if (dir.ToString() == "Left")
            return Direction = Vector2.left;
        else if (dir.ToString() == "Right")
            return Direction = Vector2.right;
        else if (dir.ToString() == "DownLeft")
            return Direction = Vector2.down + Vector2.left;
        else if (dir.ToString() == "Down")
            return Direction = Vector2.down;
        else if (dir.ToString() == "DownRight")
            return Direction = Vector2.down + Vector2.right;
        return Vector2.zero;
    }
}

public enum Direction
{
    None,
    UpLeft,
    Up,
    UpRight,
    Left,
    Right,
    DownLeft,
    Down,
    DownRight
}