﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
	public Animator animator = null;

	public float maxLife = 1.0f;
	public float currentLife = 1.0f;
	public float TimeToFlee = 1.0f;
	private Vector2 vel = Vector2.zero;
	Player player;

	public float damageOnPlayerTouch = 5.0f;


	public delegate void DamageTaken(float value);
	public event DamageTaken onDamageTaken;

	public delegate void LifeChanged(float currentLife, float maxLife);
	public event LifeChanged onLifeChanged;

	public delegate void Died();
	public event Died onDeath;

	public bool isDead = false;
	public bool loading = false;


	private void Awake()
	{
		if (animator == null)
			animator = GetComponent<Animator>();
		player = GameObject.FindObjectOfType<Player>();
	}

	void Start()
	{
		currentLife = maxLife;
		UIManager.instance.SetBoss(this);
	}

	public void TakeDamage(float value)
	{
		currentLife = Mathf.Max(currentLife - value, 0.0f);

		if (onDamageTaken != null)
			onDamageTaken(value);
		if (onLifeChanged != null)
			onLifeChanged(currentLife, maxLife);
		if (currentLife == 0.0f)
		{
			if (animator != null && transform.parent.name == "Boss5")
			{
				animator.SetTrigger("Death");
			}
			else
			{
				if (onDeath != null)
					onDeath();
			}
            foreach (Collider2D c in GetComponents<Collider2D>())
            {
                c.enabled = false;
                Debug.Log("");
            }
            TurnOfBullManager();
			foreach (Bullet bul in GameObject.FindObjectsOfType<Bullet>())
				Destroy(bul.gameObject);
			foreach (BulletPlayer bul in GameObject.FindObjectsOfType<BulletPlayer>())
				Destroy(bul.gameObject);
		}
	}

	public void Die()
	{
		isDead = true;
		if (onDeath != null)
			onDeath();
		UIManager.instance.DisplayWinScreen();
	}

	void TurnOfBullManager()
	{
		foreach (BulletManager BM in FindObjectsOfType<BulletManager>())
			BM.gameObject.SetActive(false);
		foreach (CreateBullets BM in FindObjectsOfType<CreateBullets>())
			BM.gameObject.SetActive(false);
		player.enabled = false;
	}

	private void Update()
	{
		if (Input.GetKey(KeyCode.O))
			TakeDamage(maxLife * 0.15f);

		if (currentLife <= 0 && transform.parent.name != "Boss5")
		{
            var XY = Vector2.SmoothDamp(new Vector2(transform.position.x, transform.position.y),
new Vector2(transform.position.x + 20, transform.position.y), ref vel, TimeToFlee, 1, 1);
			transform.position = new Vector3(XY.x, XY.y, transform.position.z);
			if (player.CheckLimit(transform))
			{
				SoundManager.PlayMusic(0);
				GameObject.Find("ChangeToShop").GetComponent<Button>().onClick.Invoke();
				player.enabled = true;
				player.ReplacePlayer();
				Destroy(transform.parent.gameObject);
			}
		}
		else if (currentLife <= 0.0f && transform.parent.name == "Boss5")
		{
			if (isDead == true && loading == false && (Input.GetAxisRaw("Submit") != 0.0f || Input.GetAxisRaw("Cancel") != 0.0f))
			{
				StartCoroutine(ReturnToMenu());
				SoundManager.instance.PlayValidationSound();
			}
		}
	}

    IEnumerator ReturnToMenu()
	{
		loading = true;
		yield return new WaitForSeconds(1f);
		SoundManager.PlayMusic(0);
		SceneManager.LoadScene(1);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			if (player != null)
				player.TakeDamage(damageOnPlayerTouch);
		}
	}
}
