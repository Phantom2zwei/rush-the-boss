﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public List<Button> But;
    public CanvasGroup Fade;
    public List<Text> LScore;
    int CurrentBut = 0;
    float timer = 0f;
    [HideInInspector]
    public bool canGetInput = false;
    public Animator Test;

    private void Awake()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    private void Start()
    {
        CustomPlayerPref<int>.AddEntry("currentFolderSave", 1);
        SaveLoad.ChangeFileNameAndExtension("Nirvana", "nir");

        if (LScore.Count != 0)
        {
            if (SaveLoad.SaveFileExist())
            {
                SaveScore score = SaveLoad.Load<SaveScore>("Score");
                for (int i = 0; i < score.Score.Count; i++)
                {
                    LScore[i].text = score.Score[i].ToString();
                }
            }
            else
            {
                SaveScore emptyScore = new SaveScore();
                foreach (Text t in LScore)
                    emptyScore.Score.Add(0);
                SaveLoad.AddOrModifyElementInSaveFile("Score", emptyScore);
            }
        }
        else
            Debug.Log("Their is no Score in Lscore in MainMenu script");
    }

    void Update()
    {
        if (But.Count == 0 || Fade == null)
            return;
        if (!canGetInput)
            return;
        if (Input.GetAxis("Vertical") < 0)
        {
            if (timer <= 0f)
            {
                CurrentBut = (CurrentBut + 1 < But.Count) ? CurrentBut + 1 : 0;
                timer = 0.2f;
            }
            timer -= Time.deltaTime;
        }
        else if (Input.GetAxis("Vertical") > 0)
        {
            if (timer <= 0f)
            {
                CurrentBut = (CurrentBut - 1 < 0) ? But.Count - 1 : CurrentBut - 1;
                timer = 0.2f;
            }
            timer -= Time.deltaTime;
        }
        if (Input.GetAxis("Vertical") == 0)
            timer = 0;
        But[CurrentBut].Select();
        if (Input.GetAxis("Submit") != 0)
            But[CurrentBut].onClick.Invoke();
    }

    public void LaunchGame()
    {
        StartCoroutine(WaitToLoad());
    }

    public void PlayAnimMenu()
    {
            Test.SetTrigger("Menu");
    }

    public void PlayReverse()
    {
        Test.ResetTrigger("Menu");
        Test.SetTrigger("Rewind");
    }

    IEnumerator WaitToLoad()
    {
        canGetInput = false;
        Test.SetTrigger("Menu");

        yield return new WaitForSeconds(2f);
        if (Fade != null)
            while (Fade.alpha != 1f)
            {
                Fade.alpha += Time.deltaTime / 2;
                yield return null;
            }
        SceneManager.LoadScene(2);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
}
