﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopToBoss : MonoBehaviour
{
    public List<Transform> LBoss = new List<Transform>();
    public List<Transform> ObjectToChangeActive = new List<Transform>();
    public Transform ReplacePlayer;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            if (LBoss != null)
                if (LBoss.Count > 0)
                {
                    if (LBoss[0] != null && (LBoss[0].name == "Boss5" || LBoss[0].name == "Boss4"))
                        SoundManager.PlayMusic(2);
                    else
                        SoundManager.PlayMusic(1);
                    UIManager.instance.SetStage(int.Parse(LBoss[0].name.Remove(0, 4))); // Sends the number of the current boss
                    LBoss[0].gameObject.SetActive(true);
                    LBoss.RemoveAt(0);
                    foreach (Transform obj in ObjectToChangeActive)
                        obj.gameObject.SetActive(!obj.gameObject.activeSelf);
                    if (ReplacePlayer != null)
                    {
                        Player p = GameObject.FindObjectOfType<Player>();
                        p.transform.position = ReplacePlayer.position;
                        p.hasUpgraded = false;
                    }
                }
            foreach (BulletPlayer bul in GameObject.FindObjectsOfType<BulletPlayer>())
                Destroy(bul.gameObject);
        }
    }
}
