﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
	public enum Type
	{
		Attack,
		HP,
		AttackSpeed,
		Score
	}

	public Type type;
	public float valueToAdd = 0.0f;
}
