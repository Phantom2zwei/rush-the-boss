﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    private Button b;

    private void Start()
    {
        if (GetComponent<Button>())
            b = GetComponent<Button>();
    }

    void Update ()
    {
        if (b != null)
        if (Input.GetAxis("Cancel") != 0)
                b.onClick.Invoke();
    }

    public void RetardeOnClic(Button but)
    {
        StartCoroutine(retardeOnClic(but));
    }

    IEnumerator retardeOnClic(Button but)
    {
        yield return new WaitForSeconds(3f);
        but.onClick.Invoke();
    }
}
