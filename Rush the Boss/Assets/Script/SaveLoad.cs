﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;

/*
 EX:
 SaveLoad.AddOrModifyElementInSaveFile<SerializableStringList>("1erTest", CustomClassSerialize);
 SaveLoad.EraseAllInSaveAndAdd<SerializableStringList>("2emeTest", asd);
     
 asd = SaveLoad.Load<SerializableStringList>("2emeTest");
 */


/// <summary>
/// Save and Load Class withoud monobehavior
/// and variable have to be public to be saved.
/// </summary>
public class SaveLoad
{
    private static string path;
    private static string tmpPath = path + "/tmp.tmp";
    private static string extension = ".data";
    private static string nameFile = "save";

    public static void ChangeFileNameAndExtension(string newName = "save", string newExtension = "data")
    {
        if (newExtension.Length == 0)
            return;
        for (int i = 0; i <= newExtension.Length; i++)
        {
            if (i == newExtension.Length)
                return;
            if (newExtension[i] != ' ')
                break;
        }

        if (!CheckIfPossible(newName))
        {
            Debug.LogErrorFormat("File name for save cannot be called {0}", newName);
            return;
        }

        if (!CheckIfPossible(newExtension))
        {
            Debug.LogErrorFormat("File extension for save cannot be called {0}", newExtension);
            return;
        }
        nameFile = newName;
        extension = '.' + newExtension;
    }

    private static bool CheckIfPossible(string checkChar)
    {
        foreach (char c in Path.GetInvalidFileNameChars())
            if (checkChar.Contains(c.ToString()))
                return false;
        return true;
    }

    public static bool SaveFileExist()
    {
        path = Application.persistentDataPath + "/Save" + CustomPlayerPref<int>.GetEntry("currentFolderSave");
        if (!Directory.Exists(path))
            return false;
        path = path + "/" + nameFile + extension;
        if (!File.Exists(path))
            return false;
        return true;
    }

    ///<summary>
    ///<para>Erase what is written in file and write in the wanted data</para>
    ///</summary>
    public static void EraseAllInSaveAndAdd<T>(string entryKey, T dataToSave)
    {
        path = Application.persistentDataPath + "/Save" + CustomPlayerPref<int>.GetEntry("currentFolderSave");
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        string pathToSave = path + "/" + nameFile + extension;
        string dataToJson = JsonUtility.ToJson(dataToSave);
        if (dataToJson == "{}")
        {
            Debug.LogErrorFormat(entryKey + " cannot be saved, got no data from the data to save");
            return;
        }

        File.WriteAllText(pathToSave, entryKey + " ");
        using (StreamWriter sw = File.AppendText(pathToSave))
        {
            sw.WriteLine("");
            sw.WriteLine(dataToJson);
        }
    }

    ///<summary>
    ///<para>Add data in file, if already present replace value. if not insert data</para>
    ///</summary>
    public static void AddOrModifyElementInSaveFile<T>(string entryKey, T dataToSave)
    {
        path = Application.persistentDataPath + "/Save" + CustomPlayerPref<int>.GetEntry("currentFolderSave");
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
            EraseAllInSaveAndAdd<T>(entryKey, dataToSave);
            return;
        }

        string tmpData = JsonUtility.ToJson(dataToSave);
        if (tmpData == "{}")
        {
            Debug.LogErrorFormat(entryKey + " cannot be saved, got no data from " + entryKey);
            return;
        }
        string pathToSave = path + "/" + nameFile + extension;

        if (File.Exists(pathToSave))
        {
            string[] allLineInFile = File.ReadAllLines(pathToSave);
            for (int i = 0; i < allLineInFile.Length; i += 2)
            {
                if (allLineInFile[i].Contains(entryKey))
                {
                    allLineInFile[i + 1] = tmpData;
                    tmpPath = path + "/tmp.tmp";
                    if (File.Exists(tmpPath))
                        File.Delete(tmpPath);
                    File.WriteAllLines(tmpPath, allLineInFile);
                    File.Delete(pathToSave);
                    File.Move(tmpPath, pathToSave);
                    return;
                }
            }
        }
        using (StreamWriter sw = File.AppendText(pathToSave))
        {
            sw.WriteLine(entryKey);
            sw.WriteLine(tmpData);
        }
    }

    ///<summary>
    ///<para>Search data in file, if found assign value. if not return default value</para>
    ///</summary>
    public static T Load<T>(string dataToFind)
    {
        path = Application.persistentDataPath + "/Save" + CustomPlayerPref<int>.GetEntry("currentFolderSave");
        string pathGetSave = path + "/" + nameFile + extension;
        if (!File.Exists(pathGetSave))
        {
            Debug.LogErrorFormat("Save file doesn't exist");
            return default(T);
        }

        using (StreamReader sr = new StreamReader(pathGetSave))
        {
            string[] AllLine = File.ReadAllLines(pathGetSave);
            for (int i = 0; i < AllLine.Length; i += 2)
                if (AllLine[i].Contains(dataToFind))
                    return JsonUtility.FromJson<T>(AllLine[i + 1]);
        }
        Debug.LogErrorFormat("Cannot find " + dataToFind + " in file save");
        return default(T);
    }
}

class CustomPlayerPref<T>
{
    private static List<KeyValuePair<string, T>> StoredValues = new List<KeyValuePair<string, T>>();

    public static void AddEntry(string nameV, T valueToStore)
    {
        StoredValues.Add(new KeyValuePair<string, T>(nameV, valueToStore));
    }

    public static T GetEntry(string SearchFor)
    {
        for (int i = 0; i < StoredValues.Count; i++)
            if (StoredValues[i].Key == SearchFor)
                return StoredValues[i].Value;
        Debug.LogErrorFormat("Cannot find " + SearchFor + " in CustomPlayerPref");
        return default(T);
    }
}
