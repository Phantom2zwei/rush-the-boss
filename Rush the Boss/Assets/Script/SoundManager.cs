﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
	public static SoundManager instance = null;

	[Range(0.0f, 1.0f)]
	static public float globalVolume = 1.0f;
	public List<AudioClip> musics = new List<AudioClip>();

	private AudioSource musicSource = null;
	private List<AudioSource> sfxSources = new List<AudioSource>();

	public AudioClip validationSound = null;
	public AudioClip hurtSound = null;

	public List<AudioClip> bossSounds = new List<AudioClip>();


	void Awake()
	{
		if (instance == null)
		{
			instance = this;
			musicSource = GetComponent<AudioSource>();
			musicSource.volume = globalVolume;
		}
		else
			Destroy(gameObject);
	}

	void Start()
	{
		if (musics != null && musics.Count > 0)
		{
			PlayMusic(0);
		}
		StartCoroutine(CleanSfx());
	}

	public static void PlayMusic(int id)
	{
		if (instance != null)
		{
			instance.musicSource.clip = instance.musics[id];
			instance.musicSource.Play();
		}
	}

	public static void SetVolume(float val)
	{
		if (instance != null)
		{
			instance.musicSource.volume = instance.musicSource.volume * val / globalVolume;
			foreach (AudioSource s in instance.sfxSources)
				s.volume = s.volume * val / globalVolume;
			Debug.Log(globalVolume);
		}
		globalVolume = val;
	}

	public static AudioSource PlaySfx(AudioClip clip, GameObject go, float volume = 1.0f)
	{
		if (instance != null && clip != null)
		{
			AudioSource newSource = null;
			if (go != null)
			{
				newSource = go.AddComponent<AudioSource>();
				newSource.spatialBlend = 0.0f;
			}
			else
			{
				newSource = instance.gameObject.AddComponent<AudioSource>();
				newSource.spatialBlend = 0.75f;
			}
			newSource.clip = clip;
			newSource.loop = false;
			newSource.volume = volume * globalVolume;
			newSource.Play();

			instance.sfxSources.Add(newSource);
			return newSource;
		}
		return null;
	}

	IEnumerator CleanSfx()
	{
		for (;;)
		{
			for (int i = 0; i < instance.sfxSources.Count; ++i)
			{
				if (instance.sfxSources[i].isPlaying == false)
				{
					Destroy(instance.sfxSources[i]);
					instance.sfxSources.RemoveAt(i--);
				}
			}
			yield return new WaitForSeconds(5.0f);
		}
	}

	public static void PauseSfx()
	{
		if (instance != null)
		{
			foreach (AudioSource s in instance.sfxSources)
				s.Pause();
		}
	}

	public static void UnpauseSfx()
	{
		if (instance != null)
		{
			foreach (AudioSource s in instance.sfxSources)
				s.UnPause();
		}
	}

	public void PlayValidationSound()
	{
		if (validationSound != null)
			PlaySfx(validationSound, null, 1.0f);
	}

	public void PlayHurtSound()
	{
		if (hurtSound != null)
			PlaySfx(hurtSound, null, 1.0f);
	}

	public void PlayRandomBossSound()
	{
		if (bossSounds.Count > 0)
		{
			int id = Random.Range(0, 0 + (bossSounds.Count - 1));
			PlaySfx(bossSounds[id], null, 1.0f);
		}
	}
}
