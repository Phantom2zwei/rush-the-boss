﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
	public List<GroupeBullWithTime> groups = new List<GroupeBullWithTime>();
    public float Delay = 1.5f;
    float timer = 1f;
    float Spawn = 1f;
    private CreateBullets previous;
    int rand = -1;

	public AudioClip warningSound = null;

    private void Start()
    {
        if (groups.Count >0)
            timer = groups[0].SpawnTsec + 1;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (previous)
            previous.Repeat += Time.deltaTime;

        if (timer >= Spawn)
        {
            if (rand == -1)
                rand = UnityEngine.Random.Range(0, groups.Count);

            groups[rand].GroupBullet.MoveBoss = true;
            if (previous)
            {
                previous.Repeat = 0;
                previous.MoveBoss = false;
                previous = null;
            }
            Spawn = groups[rand].SpawnTsec;
            StartCoroutine(InstantiateBullet(groups[rand]));

            timer = 0;
            //rand = UnityEngine.Random.Range(0, groups.Count);
            
        }
    }

    IEnumerator InstantiateBullet(GroupeBullWithTime groupe)
    {
        if (groups.Count > 1)
            while (groups[rand].GroupBullet == groupe.GroupBullet)
                rand = UnityEngine.Random.Range(0, groups.Count);
        if (groups[rand].Warning != null)
            StartCoroutine(ShowWarning(groups[rand]));
        yield return new WaitForSeconds(Delay);
        groupe.GroupBullet.InstBull();
        previous = groupe.GroupBullet;
				SoundManager.instance.PlayRandomBossSound();
    }

    IEnumerator ShowWarning(GroupeBullWithTime groupe)
    {
				if (warningSound != null)
						SoundManager.PlaySfx(warningSound, null, 1.0f);
        groupe.Warning.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        groupe.Warning.gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        groupe.Warning.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        groupe.Warning.gameObject.SetActive(false);
    }
}

[Serializable]
public class GroupeBullWithTime
{
    public CreateBullets GroupBullet;
    public Transform Warning;
    public float SpawnTsec = 0.5f;
}
