﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeMainMenu : MonoBehaviour
{
    public float WaitSec = 2;
    CanvasGroup thisImage;
    public MainMenu menu;

    void Start()
    {
        thisImage = gameObject.GetComponent<CanvasGroup>();
        if (menu)
            StartCoroutine(Fondu());
        else
            StartCoroutine(founduIngame());
    }

    IEnumerator Fondu()
    {
        if (menu)
            menu.PlayReverse();
        yield return new WaitForSeconds(1f);

        while (thisImage.alpha != 0f)
        {
            thisImage.alpha -= Time.deltaTime / 2;
            yield return null;
        }

        if (menu)
        {
            menu.canGetInput = true;
            if (menu.But.Count > 0)
                menu.But[0].Select();
        }
    }
    IEnumerator founduIngame()
    {
        while (thisImage.alpha != 0f)
        {
            thisImage.alpha -= Time.deltaTime / 2;
            yield return null;
        }
    }
}
