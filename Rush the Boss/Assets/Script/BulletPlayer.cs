﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{
	public float moveSpeed = 1.0f;

	[HideInInspector]
	public Vector2 Direction = Vector2.zero;
	public float damage = 1.0f;
	Player pl;

	private void Start()
	{
		pl = GameObject.FindObjectOfType<Player>();
	}

	private void Update()
	{
		transform.Translate(Direction.normalized * moveSpeed * Time.deltaTime);
		if (pl.CheckLimit(transform))
			Destroy(gameObject);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.transform.GetComponent<Boss>() != null)
		{
			Boss b = collision.transform.GetComponent<Boss>();
			b.TakeDamage(damage);
			transform.Translate(Direction * moveSpeed * 1000 * Time.deltaTime);
			if (pl != null)
				pl.AddScore(damage);
		}
	}
}

