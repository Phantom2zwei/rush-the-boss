﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class SaveScore
{
    public List<int> Score = new List<int>();
}
