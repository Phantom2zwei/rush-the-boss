﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
    public CanvasGroup Isart;
    public CanvasGroup Ours;
    public CanvasGroup Warning;
    public CanvasGroup MovieCanvas;
    public RawImage Movie;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        StartCoroutine(FonduIsart());
    }

    IEnumerator FonduIsart()
    {
        while (Isart.alpha != 1)
        {
            Isart.alpha += Time.deltaTime/2;
            yield return null;
        }

        while (Isart.alpha != 0)
        {
            Isart.alpha -= Time.deltaTime/2;
            yield return null;
        }
        StartCoroutine(FonduOurs());
    }

    IEnumerator FonduOurs()
    {
        while (Ours.alpha != 1)
        {
            Ours.alpha += Time.deltaTime/2;
            yield return null;
        }

        while (Ours.alpha != 0)
        {
            Ours.alpha -= Time.deltaTime/2;
            yield return null;
        }
        StartCoroutine(FonduWarning());
    }

    IEnumerator FonduWarning()
    {
        while (Warning.alpha !=1)
        {
            Warning.alpha += Time.deltaTime/2;
            yield return null;
        }

        while (Warning.alpha != 0)
        {
            Warning.alpha -= Time.deltaTime/3;
            yield return null;
        }
        StartCoroutine(FonduMovie());
    }

    IEnumerator FonduMovie()
    {
        MovieTexture movie = (MovieTexture)Movie.mainTexture;
        movie.Play();
        yield return new WaitForSeconds(0.5f);
        movie.Pause();

        while (MovieCanvas.alpha != 1)
        {
            MovieCanvas.alpha += Time.deltaTime / 2;
            yield return null;
        }

        movie.Play();
        yield return new WaitForSeconds(8f);

        while (MovieCanvas.alpha != 0)
        {
            MovieCanvas.alpha -= Time.deltaTime / 3;
            yield return null;
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
}
