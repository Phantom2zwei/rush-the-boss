﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float moveSpeed = 1.0f;

	[HideInInspector]
	public Vector2 Direction = Vector2.zero;
	public float damage = 1.0f;

	private void Update()
	{
		transform.Translate(Direction * moveSpeed * Time.deltaTime);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.transform.GetComponent<Player>() != null)
		{
			Player p = collision.transform.GetComponent<Player>();
			p.TakeDamage(damage);
        }
    }
}

